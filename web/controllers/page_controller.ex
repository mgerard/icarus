defmodule Reviews.PageController do
  use Reviews.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
